use prost::Message;
use measure_handler::schema::Measure;
use std::thread;
use std::time::{Instant, Duration};

mod measure_handler;

fn main() {
    loop {
        let start_time = Instant::now();

        let measure = Measure::generate_random("hostname");
        let _serialized_message: Vec<u8> = measure.encode_to_vec();

        let duration = Instant::now().duration_since(start_time);
        println!("Serialization duration: {} milliseconds", duration.as_millis());
        thread::sleep(Duration::from_secs(10));
        println!("{:?}",measure);
    }
}
