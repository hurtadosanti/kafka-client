pub mod schema{
    include!(concat!(env!("OUT_DIR"), "/schema.rs"));
}
use schema::Measure;
use rand::{Rng, thread_rng};
use std::time::{SystemTime, UNIX_EPOCH};
impl Measure {
    // Generate a random Measure instance
    pub fn generate_random(device: &str) -> Measure {

        let mut rng = thread_rng();
        let now = SystemTime::now();

        let latitude = rng.gen_range(-90.0..90.0);
        let longitude = rng.gen_range(-180.0..180.0);

        let temperature = rng.gen_range(15.0..30.0);
        let humidity = rng.gen_range(0.0..100.0);
        let pressure = rng.gen_range(900.0..1100.0);
        let duration = now.duration_since(UNIX_EPOCH).expect("Time can not be created");
        let measure_time = duration.as_secs();
        let device_id = String::from(device);

        Measure {
            measure_time,
            device_id,
            latitude,
            longitude,
            temperature,
            humidity,
            pressure,
        }
    }
}